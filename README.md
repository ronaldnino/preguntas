# Léeme #

Este repositorio contiene las preguntas y respuesta de la “Prueba tecnica  BackEnd Rappi”

###  ¿En qué consiste el principio de responsabilidad única? ¿Cuál es su propósito? ###

Consiste en establecer la responsabilidad de cada modulo o clase sobre una sola parte de la
funcionalidad proporcionada por el software y esta responsabilidad debe estar totalmente
encasulada por la clase. El proposito de este principio es escribir software de calidad,gracias a
este se podra crear codigo con mayor facilidad de lectura, de prueba y mantenimiento.

### ¿Qué características tiene según su opinión “buen” código o código limpio? ###

Tiene que ser enfocado, terner solucione simples, facil de leer,
reutilizable, respectar convención de codigo para que otro programador
pueda extenderlo facilmente, debe tener pruebas unitarias y de aceptación,
debe ser expresivo.

### ¿Qué es un microservicio?, ventajas y desventajas de los micorservicios ###

Es una arquitectura que permite crear soluciones de software mediante un conjunto de
servicios independientes que se despliguegan según se vayan necesitando. Con el fin de
tener una aplicación modular a base de pequeñas piezas, que podriamos ir ampliando o
reduciendo a medida que se requiera.
### Ventajas: ###
* Otorga la liberta de desarrollar y de desplegar servicios de forma independiente.
* Se puede desarrollar con un equipo de trabajo mínimo.
* Se puede usar diferentes lenguajes de programación en diferentes modulos.
* Facil de integrar y despliegue automático.
* Facil de integrar con aplicaciones de terceros
###  Desventajas: ###
* Dificulta para la realización de pruebas.
* Un gran número de servicio pueden dar lugar a grades bloques de información que gestionar.
* Sera labor de los desarrolladores lidiar con aspectos como la latencia de la red, tolerancia a fallos, balanceo de carga, entre otros problemas en el ecosistema.
* Alto consumo de memoria.

